package pl.edu.pwsztar.game.checker;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.game.ChessBoard;
import pl.edu.pwsztar.game.model.FigurePosition;

@Component
public class BishopMovesChecker implements MovesChecker<FigureType> {

    @Override
    public FigureType getType() {
        return FigureType.BISHOP;
    }

    @Override
    public boolean isCorrect(FigurePosition begin, FigurePosition destination) {

        final int beginX = ChessBoard.getXIndex(begin.getX());
        final int destinationX = ChessBoard.getXIndex(destination.getX());

        final int beginY = ChessBoard.getYIndex(begin.getY());
        final int destinationY = ChessBoard.getYIndex(destination.getY());

        return Math.abs(beginX - destinationX) == Math.abs(beginY - destinationY);
    }
}
