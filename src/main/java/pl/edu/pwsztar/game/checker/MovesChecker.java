package pl.edu.pwsztar.game.checker;

import pl.edu.pwsztar.game.model.FigurePosition;

public interface MovesChecker<T extends Enum<T>> {

    T getType();

    boolean isCorrect(FigurePosition begin, FigurePosition destination);
}
