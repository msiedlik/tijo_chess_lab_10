package pl.edu.pwsztar.game.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.mapper.InternalMapper;
import pl.edu.pwsztar.game.model.FigurePosition;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class FigurePositionMapper implements InternalMapper<String, FigurePosition> {

    @Override
    public FigurePosition map(String original) {
        final List<String> positions = Pattern.compile("_")
                .splitAsStream(original)
                .collect(Collectors.toList());

        return new FigurePosition(positions.get(0), positions.get(1));
    }
}
