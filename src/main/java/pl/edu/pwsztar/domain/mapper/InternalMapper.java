package pl.edu.pwsztar.domain.mapper;

public interface InternalMapper<T, U> {

    U map(T original);
}
