package pl.edu.pwsztar.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.domain.mapper.InternalMapper;
import pl.edu.pwsztar.game.checker.MovesChecker;
import pl.edu.pwsztar.game.model.FigurePosition;

import java.util.List;

@Service
public class ChessServiceImpl implements ChessService {

    private final InternalMapper<String, FigurePosition> positionInternalMapper;

    private final List<MovesChecker<FigureType>> checkers;

    @Autowired
    public ChessServiceImpl(
            InternalMapper<String, FigurePosition> positionInternalMapper,
            List<MovesChecker<FigureType>> checkers
    ) {
        this.positionInternalMapper = positionInternalMapper;
        this.checkers = checkers;
    }

    @Override
    public boolean isMoveCorrect(FigureMoveDto figureMoveDto) {
        final Pair<FigurePosition, FigurePosition> figurePositionPair = getBeginAndDestinationPair(figureMoveDto);

        if (figurePositionPair.getFirst().equals(figurePositionPair.getSecond())) {
            return false;
        }

        return findByDto(figureMoveDto).isCorrect(
                figurePositionPair.getFirst(),
                figurePositionPair.getSecond()
        );
    }

    private MovesChecker<FigureType> findByDto(FigureMoveDto figureMoveDto) {
        return checkers
                .stream()
                .filter(checker -> figureMoveDto.getType().equals(checker.getType()))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
    }

    private Pair<FigurePosition, FigurePosition> getBeginAndDestinationPair(FigureMoveDto figureMoveDto) {
        return Pair.of(
                positionInternalMapper.map(figureMoveDto.getStart()),
                positionInternalMapper.map(figureMoveDto.getDestination())
        );
    }
}
